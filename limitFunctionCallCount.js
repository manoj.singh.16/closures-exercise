const limitFunctionCallCount = (cb, n) => {
  // Should return a function that invokes `cb`.
  // The returned function should only allow `cb` to be invoked `n` times.
  // Returning null is acceptable if cb can't be returned

  // if callback and `n` is not passed to the function then i will return an anonymous funtion which always returns null.

  if (!n || !cb) return () => null;

  let counter = 0;

  const execute = (...parameter) => {
    if (counter < n) {
      counter++;
      return cb(...parameter);
    }
    return null;
  };

  return execute;
};

module.exports = limitFunctionCallCount;
