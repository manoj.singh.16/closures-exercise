const cacheFunction = require("../cacheFunction");

const cb = (n, m) => {
  console.log("pp");
  return n + m;
};

const result = cacheFunction(cb);

console.log(result(2, 4)); // invokes callback
console.log(result(2, 4)); // returns cached result
console.log(result(2, 5)); // invokes callback
console.log(result(2, 5, 6)); // invokes callback
