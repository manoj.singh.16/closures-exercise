const limitFunctionCallCount = require("../limitFunctionCallCount");

const cb = (n, m) => n * m;

// if callback and `n` is not passed to the function then i will return an anonymous funtion which always returns null.

const result = limitFunctionCallCount(cb, 2);

console.log(result(2, 3)); // 1 executes
console.log(result(3, 4)); // 2 executes
console.log(result(2, 3)); // returns null
