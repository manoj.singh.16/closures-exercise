const counterFactory = require("../counterFactory");

const result = counterFactory();

console.log(result.increment()); // increments by 1
console.log(result.increment(5)); // incremnets by val (5)
console.log(result.decrement()); // decrements by 1
console.log(result.decrement(2)); // decrements by val (2)
