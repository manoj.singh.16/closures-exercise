const cacheFunction = (cb) => {
  // Should return a function that invokes `cb`.
  // A cache (object) should be kept in closure scope.
  // The cache should keep track of all arguments have been used to invoke this function.
  // If the returned function is invoked with arguments that it has already seen
  // then it should return the cached result and not invoke `cb` again.
  // `cb` should only ever be invoked once for a given set of arguments.

  if (!cb) return () => null;

  const cache = {};

  if (!cb) return;

  const execute = (...parameters) => {
    const key = JSON.stringify(parameters); // here we are just converting parameter array to string to use as key. instead of this we could have used some hashing function.
    if (cache[key]) return cache[key];

    cache[key] = cb(...parameters);
    return cache[key];
  };

  return execute;
};

module.exports = cacheFunction;
